package me.jake.spigotresourceapi;

import java.io.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Jake on 1/5/2021.
 * <insert description here>
 */
public class SpigotResourceUtil {

    //How many resources should be pinged
    public static final int MAX_RESOURCES = 80000;
//    public static final int MAX_RESOURCES = 50;

    static ConcurrentHashMap<Integer, ResourceInfo> data = new ConcurrentHashMap<>();
    private static final File dataFile = new File("public/cache.dat");

    static void readData() {
        if (!dataFile.exists()) {
            System.err.println("Data file not found");
            return;
        }
        try {
            DataInputStream input = new DataInputStream(new FileInputStream(dataFile));
            int amount = input.readInt();
            for (int i = 0; i < amount; i++) {
                ResourceInfo resourceInfo = ResourceInfo.fromData(input);
                data.put(resourceInfo.getId(), resourceInfo);
            }
        } catch (FileNotFoundException e) {
            System.err.println("File not found (first run?)");
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static void writeData() {
        try {
            DataOutputStream out = new DataOutputStream(new FileOutputStream(dataFile));
            out.writeInt(data.size());
            for (ResourceInfo value : data.values()) {
                value.serialize(out);
            }
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    static AtomicInteger totalCount = new AtomicInteger(0);
    static ConcurrentLinkedQueue<Thread> threads = new ConcurrentLinkedQueue<>();
    public static void main(String[] args) throws InterruptedException {
        readData();
        int THREAD_COUNT = 20;
        for (int i = 0; i < THREAD_COUNT; i++) {
            int count = MAX_RESOURCES / THREAD_COUNT;
            int start = i * count;
            System.err.println("DISPATCH THREAD: " + i + ", start=" + start + ", count=" + count);
            Thread.sleep(85);
            Thread thr = new Thread() {
                @Override
                public void run() {
                    for (int j = start; j < start + count; j++) {
                        String json = SpigotResourceNet.getJson(j);
                        if (json == null) {
                            //No data (404)
                            int total = totalCount.incrementAndGet();
                        } else if (json.equals("T")) {
                            //Connection throttled, continue
                            j--;
                        } else {
                            int total = totalCount.incrementAndGet();
                            ResourceInfo resourceInfo = ResourceInfo.fromJson(json);
                            System.err.println("["+total+"/"+MAX_RESOURCES+"] DL::" + resourceInfo.getId() + " ==> " + resourceInfo.getTitle());
                            data.put(resourceInfo.getId(), resourceInfo);
                        }
                    }
                }
            };
            threads.add(thr);

            thr.start();
        }

//        for (int i = 0; i < MAX_RESOURCES; i++) {
////            int id = incrementLastUpdatedId();
//            String json = SpigotResourceNet.getJson(i);
//            if(json != null) {
//                ResourceInfo resourceInfo = ResourceInfo.fromJson(json);
//                System.err.println("DL::" + resourceInfo.getId() + " ==> " + resourceInfo.getTitle());
//                data.put(resourceInfo.getId(), resourceInfo);
//            }else{
//
//            }
//        }
        while (true){
            boolean oneAlive = false;
            for (Thread thread : threads) {
                if (thread.isAlive()) {
                    oneAlive = true;
                }
            }
            if(!oneAlive) {
                writeData();
                break;
            }
        }
        dumpData();
    }

    static void dumpData() {
        System.out.println("======= BEGIN DUMP =======");
        for (ResourceInfo value : data.values()) {
            System.out.println(value);
        }

        System.out.println("==========================");
    }



    private static final File idFile = new File("public/lastid.dat");
}
