package me.jake.spigotresourceapi;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Created by Jake on 1/5/2021.
 * <insert description here>
 */
public class ResourceInfo {
    private int id;
    private String title;
    private String tag;
    private String currentVersion;

    private int authorId;
    private String authorUsername;

    private float price;
    private String currency;

    private int downloads;
    private int updates;
    private int reviews;
    private float rating;

    static ResourceInfo fromJson(String raw){
        JsonElement elem = JsonParser.parseString(raw);
        JsonObject o = elem.getAsJsonObject();
        ResourceInfo info = new ResourceInfo();
        info.id = o.get("id").getAsInt();
        info.title = o.get("title").getAsString();
        info.tag = o.get("tag").getAsString();
        info.currentVersion = o.get("current_version").getAsString();

        JsonObject authorObject = o.get("author").getAsJsonObject();
        info.authorId = authorObject.get("id").getAsInt();
        info.authorUsername = authorObject.get("username").getAsString();

        JsonObject premiumObject = o.get("premium").getAsJsonObject();
        info.price = premiumObject.get("price").getAsFloat();
        info.currency = premiumObject.get("currency").getAsString();

        JsonObject stats = o.get("stats").getAsJsonObject();
        info.downloads = stats.get("downloads").getAsInt();
        info.updates  = stats.get("updates").getAsInt();
        info.reviews = stats.get("reviews").getAsInt();
        info.rating = stats.get("rating").getAsFloat();

        return info;
    }
    static ResourceInfo fromData(DataInputStream stream) throws IOException {
        ResourceInfo resourceInfo = new ResourceInfo();
        resourceInfo.deserialize(stream);
        return resourceInfo;
    }

    @Override
    public String toString() {
        return "ResourceInfo{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", tag='" + tag + '\'' +
                ", currentVersion='" + currentVersion + '\'' +
                ", authorId=" + authorId +
                ", authorUsername='" + authorUsername + '\'' +
                ", price=" + price +
                ", currency='" + currency + '\'' +
                ", downloads=" + downloads +
                ", updates=" + updates +
                ", reviews=" + reviews +
                ", rating=" + rating +
                '}';
    }

    void serialize(DataOutputStream out) throws IOException {
        out.writeInt(id);
        out.writeUTF(title);
        out.writeUTF(tag);
        out.writeUTF(currentVersion);
        out.writeInt(authorId);
        out.writeUTF(authorUsername);
        out.writeFloat(price);
        out.writeUTF(currency);
        out.writeInt(downloads);
        out.writeInt(updates);
        out.writeInt(reviews);
        out.writeFloat(rating);
    }
    void deserialize(DataInputStream in) throws IOException {
        id = in.readInt();
        title = in.readUTF();
        tag = in.readUTF();
        currentVersion = in.readUTF();
        authorId = in.readInt();
        authorUsername = in.readUTF();
        price = in.readFloat();
        currency = in.readUTF();
        downloads = in.readInt();
        updates = in.readInt();
        reviews = in.readInt();
        rating = in.readFloat();
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }
}
